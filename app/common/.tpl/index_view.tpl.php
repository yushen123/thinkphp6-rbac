<?php
//字段类型白名单
$types          = [
    'int', 'bigint', 'integer', 'varchar', 'char', 'datetime',
];
$data           = [];
$tableColHtml   = '';
$tableHeadHtml  = '';
$tableFieldHtml = '';
if (isset($fieldList)) {
    foreach ($fieldList as $key => $item) {
        $t = explode('(', $item['Type']);
        if (isset($t[1])) {
            $fieldType  = $t[0];
        } else {
            $fieldType  = $item['Type'];
        }
        $t1 = explode('[', $item['Comment']);
        if (isset($t1[1])) {
            $extendType = rtrim($t1[1], ']');
        } else {
            $extendType = '';
        }

        if (strpos($item['Comment'], '-') !== 0 && in_array($fieldType, $types) && $item['Extra'] != 'auto_increment') {
            $id             = 'k' . $key;
            $fieldTitle     = $item['Comment'] ? $item['Comment'] : $item['Field'];
            $tmp            = explode('[', $fieldTitle);
            $fieldTitle     = isset($tmp[1]) ? $tmp[0] : $fieldTitle;
            $data[]         = ['field' => $item['Field'], 'title' => $fieldTitle, 'type' => $fieldType, 'extend_type' => $extendType, 'id' => $id];
            $tableColHtml   .= in_array($fieldType,['datetime'])?"\n                <col width='200'>":"\n                <col>";
            $tableHeadHtml  .= "\n                    <th>" . $fieldTitle . "</th>";
            $tableFieldHtml .= "\n                    <td>{\$item['" . $item['Field'] . "']}</td>";
        }
    }
}
?>

{include file="common/header" title="列表"}
<div class="layui-body">
    <!-- 内容主体区域 -->

    <div class="layui-box" style="padding: 20px;">
        <button type="button" id="add" class="layui-btn layui-btn-normal">添加</button>
    </div>
    <div>
        <form class="layui-form" action="" method="GET" lay-filter="component-form-group">

            <div class="layui-form-item">
                <?php
                foreach ($data as $item){
                    ?>

                <div class="layui-inline">
                    <label class="layui-form-label"><?=$item['title']?></label>
                    <div class="layui-input-inline">
                        <input id="<?=$item['id']?>" type="text" name="<?=$item['field']?>" value="{:isset($_GET['<?=$item['field']?>'])?$_GET['<?=$item['field']?>']:''}"
                            autocomplete="off" class="layui-input">
                    </div>
                </div>
                <?php }?>
            </div>

            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" lay-submit="" lay-filter="component-form-demo1">搜索</button>
                </div>
            </div>
        </form>
    </div>
    <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
        <legend>列表</legend>
    </fieldset>
    <div style="padding: 30px;">
        <table class="layui-table" lay-skin="row">
            <colgroup><?=$tableColHtml?>

                <col width="100">
            </colgroup>
            <thead>
                <tr><?=$tableHeadHtml?>

                    <th>操作</th>
                </tr>
            </thead>
            <tbody>
                {volist name="$list" id="item"}
                <tr><?=$tableFieldHtml?>

                    <td><a data-id="{$item['id']}" class="editBtn" href="javascript:"><i
                                class="layui-icon layui-icon-edit"></i> 编辑</a></td>
                </tr>
                {/volist}
            </tbody>
        </table>
    </div>
    <div>{$page|raw}</div>
</div>
<script>
    layui.use(['layer', 'form','laydate'], function () {
        var layer = layui.layer;
        var $ = layui.jquery;
        var laydate = layui.laydate;

        <?php
        foreach ($data as $item) {
            if ($item['type'] == 'datetime') {
                echo "
        //时间范围
        laydate.render({
            elem: '#{$item['id']}'
            ,type: 'datetime'
            ,range: ','
        });
        $('#{$item['id']}').attr('readonly','readonly');
            ";
            }
        }
        ?>

        //添加
        $(document).on('click', '#add', function () {
            layer.open({
                type: 2,
                title: '添加',
                area: ['90%', '99%'],
                content: './add'
            });
        })

        //编辑
        $(document).on('click', '.editBtn', function () {
            var id = $(this).data('id')
            layer.open({
                type: 2,
                title: '编辑',
                area: ['90%', '99%'],
                content: './edit?id=' + id
            });
        })
    });
</script>
{include file="common/footer"}