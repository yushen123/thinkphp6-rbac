<?php
$types = [
    'int' => '["=", "{value}"]',
    'bigint' => '["=", "{value}"]',
    'integer' => '["=", "{value}"]',
    'varchar' => '["like", "{value}%"]',
    'char' => '["like", "{value}%"]',
    'longtext' => '["like", "{value}%"]',
    'datetime' => '["between", "{value}"]',
];
?>
<?php echo "<?php \n";?>
declare (strict_types = 1);

namespace app\common\model;

class <?php echo $name;?> extends Base
{
    //查询条件
    protected $whereField = [
<?php
if (is_array($fieldList)) {
    foreach ($fieldList as $field) {
        $tmp = explode('(', $field['Type']);
        if (isset($types[$tmp[0]])) {
            echo "        '{$field['Field']}' => {$types[$tmp[0]]},\n";
        }else{
            echo "        '{$field['Field']}' => [\"like\",\"{value}%\"],\n";
        }
    }
}
?>
        'phone'    => ["like", "{value}%"],
    ];

}
