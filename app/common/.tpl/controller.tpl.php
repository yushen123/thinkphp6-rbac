<?php echo "<?php \n";?>
declare (strict_types = 1);

namespace app\admin\controller;


use app\Request;
use think\facade\View;

class <?php echo $name;?> extends AdminBase
{
    //首页
    public function index(Request $request)
    {
        $<?php echo lcfirst($name);?>Model = new \app\common\model\<?php echo $name;?>();

        $where = $<?php echo lcfirst($name);?>Model->buildWhere($request->get());
        $list  = $<?php echo lcfirst($name);?>Model->where($where)->order('id desc')->paginate(config('page.listRows'));
        View::assign('list', $list);
        View::assign('page', $list->render());
        return View::fetch();
    }

    //添加
    public function add(Request $request)
    {
        if ($request->isPost()) {
            try {
                $data = $request->post();
                //规则验证
                validate(\app\validate\<?php echo $name;?>::class)->check($data);
                $<?php echo lcfirst($name);?>Model = new \app\common\model\<?php echo $name;?>();
                $rs        = $<?php echo lcfirst($name);?>Model->save($data);
                if ($rs) {
                    return $this->success('添加成功');
                } else {
                    return $this->error('添加失败');
                }
            } catch (\Exception $e) {
                return $this->error($e->getMessage());
            }
        } else {

            return View::fetch();
        }
    }

    //编辑
    public function edit(Request $request)
    {
        $id         = $request->request('id', 0, 'intval');
        $<?php echo lcfirst($name);?>Model = new \app\common\model\<?php echo $name;?>();
        $<?php echo lcfirst($name);?>Info  = $<?php echo lcfirst($name);?>Model->find($id);
        if (!$<?php echo lcfirst($name);?>Info) {
            $this->error('数据不存在');
        }
        if ($request->isPost()) {
            $data = $request->post();
            try {
                //验证编辑字段
                validate(\app\validate\<?php echo $name;?>::class)->only(array_keys($data))->check($data);
                $rs = $<?php echo lcfirst($name);?>Model->where(['id' => $id])->update($data);
                if ($rs) {
                    return $this->success('编辑成功');
                } else {
                    return $this->success('无更新内容');
                }
            } catch (\Exception $e) {
                return $this->error($e->getMessage());
            }
        } else {
            View::assign('<?php echo lcfirst($name);?>Info', $<?php echo lcfirst($name);?>Info);
            return View::fetch();
        }
    }

    public function delete(Request $request)
    {
        $id = $request->post('id', 0, 'intval');
        $<?php echo lcfirst($name);?>Model = new \app\common\model\<?php echo $name;?>();
        $<?php echo lcfirst($name);?>Info  = $<?php echo lcfirst($name);?>Model->find($id);
        if (!$<?php echo lcfirst($name);?>Info) {
            return $this->error('数据不存在');
        }
        if ($<?php echo lcfirst($name);?>Info->delete()) {
            return $this->success('删除成功');
        } else {
            return $this->error('删除失败');
        }
    }
}