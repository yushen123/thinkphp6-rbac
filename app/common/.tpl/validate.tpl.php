<?php
$data  = [];
if (isset($fieldList)) {
    foreach ($fieldList as $key => $item) {
        $t = explode('(', $item['Type']);
        if (isset($t[1])) {
            $fieldType = $t[0];
            $length = intval($t[1]);
        } else {
            $fieldType = $item['Type'];
            $length = 0;
        }
        $t1 = explode('[', $item['Comment']);
        if (isset($t1[1])) {
            $extendType = rtrim($t1[1], ']');
        } else {
            $extendType = '';
        }

        if (strpos($item['Comment'], '-') !== 0 && $item['Extra'] != 'auto_increment') {
            $id         = 'k' . $key;
            $fieldTitle = $item['Comment'] ? $item['Comment'] : $item['Field'];
            $tmp        = explode('[', $fieldTitle);
            $fieldTitle = isset($tmp[1]) ? $tmp[0] : $fieldTitle;
            $require    = $item['Null'] == 'NO' ? 1 : 0;
            $data[]     = ['field' => $item['Field'], 'title' => $fieldTitle, 'type' => $fieldType, 'extend_type' => $extendType, 'require' => $require, 'id' => $id,'length'=>$length];
        }
    }
}
?>
<?php echo "<?php \n";?>
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class <?php echo $name;?> extends Validate
{
    protected $rule = [
<?php
foreach ($data as $field) {
    $rule = '';
    if ($field['require']==1) {
        $rule .= 'require|';
    }
    $type = $this->getRuleType($field['type']);
    if ($type) {
        $rule .=  $type.'|';
    }
    if(intval($field['length'])){
        $rule .= 'max:'.$field['length'].'|';
    }
    $rule = rtrim($rule,'|');
    if($rule){
        echo "        '{$field['field']}' => '{$rule}',\n";
    }
}

?>
    ];

    protected $message =[
<?php
foreach ($data as $field) {
    if ($field['require'] == 1) {
        echo "        '{$field['field']}.require' => '{$field['title']}不能空',\n";
    }
    if(intval($field['length'])){
        echo "        '{$field['field']}.max' => '{$field['title']}长度不能超过{$field['length']}个字符',\n";
    }
}

?>
    ];
}