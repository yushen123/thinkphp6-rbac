<?php
//字段类型白名单
$types = [
    'int', 'bigint', 'integer', 'varchar', 'char', 'datetime',
];
$data  = [];
if (isset($fieldList)) {
    foreach ($fieldList as $key => $item) {
        $t = explode('(', $item['Type']);
        if (isset($t[1])) {
            $fieldType = $t[0];
            $length    = intval($t[1]);
        } else {
            $fieldType = $item['Type'];
            $length    = 0;
        }
        $t1 = explode('[', $item['Comment']);
        if (isset($t1[1])) {
            $extendType = rtrim($t1[1], ']');
        } else {
            $extendType = '';
        }

        if (strpos($item['Comment'], '-') !== 0 && in_array($fieldType, $types) && $item['Extra'] != 'auto_increment' && strpos($fieldType, 'date') === false) {
            $id         = 'k' . $key;
            $fieldTitle = $item['Comment'] ? $item['Comment'] : $item['Field'];
            $tmp        = explode('[', $fieldTitle);
            $fieldTitle = isset($tmp[1]) ? $tmp[0] : $fieldTitle;
            $require    = $item['Null'] == 'NO' ? 1 : 0;
            $data[]     = ['field' => $item['Field'], 'title' => $fieldTitle, 'type' => $fieldType, 'extend_type' => $extendType, 'require' => $require, 'id' => $id, 'length' => $length];
        }
    }
}
?>

{include file="common/mini-header"}
<div style="margin:2% 8%;">
    <!-- 内容主体区域 -->

    <form class="layui-form" action="" style="max-width: 800px;width: 90%;">
        <?php
        foreach ($data as $item){
        ?>
        <div class="layui-form-item">
            <label class="layui-form-label"><?=$item['require']?'* ':''?><?=$item['title']?></label>
            <div class="layui-input-block">
                <input type="text" name="<?=$item['field']?>" value="{$<?php echo lcfirst($name);?>Info['<?=$item['field']?>']}" <?=$item['require']?'required lay-verify="required"':''?>  placeholder="" autocomplete="off" class="layui-input">
            </div>
        </div>
        <?php }?>

        <div class="layui-form-item">
            <div class="layui-input-block">
                <input type="hidden" id="id" name="id" value="{$<?php echo lcfirst($name);?>Info['id']}">
                <button class="layui-btn" lay-submit lay-filter="editBtn">编辑</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                <button class="layui-btn layui-btn-danger layui-btn-xs" type="button" style="float:right" id="delBtn"><i class="layui-icon"></i> 删除</button>
            </div>
        </div>
    </form>

</div>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script>
    layui.use(['form', 'jquery'], function () {
        var form = layui.form;
        var $ = layui.jquery;
        var layer = layui.layer;
        var id = $('#id').val();

        //监听提交
        form.on('submit(editBtn)', function (data) {
            $.post('', data.field, function (rs) {
                if (rs.code == 1) {
                    parent.location.reload();
                } else {
                    layer.alert(rs.msg)
                }
            })
            return false;
        });

        //删除
        $(document).ready(function () {
            $('#delBtn').on('click', function () {
                layer.confirm('确定删除吗??', function (index) {
                    layer.close(index);
                    if (index == 1) { //确定
                        $.ajax({
                            url: './delete',
                            type: 'POST',
                            data: { id: id },
                            success: function (rs) {
                                layer.msg(rs.msg)
                                if (rs.code == 1) {
                                    setTimeout(function () {
                                        parent.location.reload();
                                    }, 400)
                                }
                            }
                        });
                    }
                });
            })
        })
    });
</script>

{include file="common/footer"}

{include file="common/footer"}