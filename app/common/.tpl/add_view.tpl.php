<?php
//字段类型白名单
$types = [
    'int', 'bigint', 'integer', 'varchar', 'char', 'datetime',
];
$data  = [];
if (isset($fieldList)) {
    foreach ($fieldList as $key => $item) {
        $t = explode('(', $item['Type']);
        if (isset($t[1])) {
            $fieldType = $t[0];
            $length    = intval($t[1]);
        } else {
            $fieldType = $item['Type'];
            $length    = 0;
        }
        $t1 = explode('[', $item['Comment']);
        if (isset($t1[1])) {
            $extendType = rtrim($t1[1], ']');
        } else {
            $extendType = '';
        }

        if (strpos($item['Comment'], '-') !== 0 && in_array($fieldType, $types) && $item['Extra'] != 'auto_increment' && strpos($fieldType, 'date') === false) {
            $id         = 'k' . $key;
            $fieldTitle = $item['Comment'] ? $item['Comment'] : $item['Field'];
            $tmp        = explode('[', $fieldTitle);
            $fieldTitle = isset($tmp[1]) ? $tmp[0] : $fieldTitle;
            $require    = $item['Null'] == 'NO' ? 1 : 0;
            $data[]     = ['field' => $item['Field'], 'title' => $fieldTitle, 'type' => $fieldType, 'extend_type' => $extendType, 'require' => $require, 'id' => $id, 'length' => $length];
        }
    }
}
?>

{include file="common/mini-header"}
<div style="margin:2% 8%;">
    <!-- 内容主体区域 -->

    <form class="layui-form" action="" style="max-width: 800px;width: 100%;">
        <?php
        foreach ($data as $item){
        ?>
        <div class="layui-form-item">
            <label class="layui-form-label"><?=$item['require']?'* ':''?><?=$item['title']?></label>
            <div class="layui-input-block">
                <input id="<?=$item['id']?>" type="text" name="<?=$item['field']?>" <?=$item['require']?'required lay-verify="required"':''?> placeholder="" autocomplete="off" class="layui-input">
            </div>
        </div>
        <?php }?>

        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit lay-filter="addBtn">添加</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>
    </form>

</div>
<script>
    layui.use(['form','laydate'], function() {
        var form = layui.form;
        var $ = layui.jquery;
        var layer = layui.layer;
        var laydate = layui.laydate;

        <?php
        foreach ($data as $item) {
            if ($item['type'] == 'datetime') {
                echo "
        //时间范围组件
        laydate.render({
            elem: '#{$item['id']}'
            ,type: 'datetime'
            ,range: ','
        });
        $('#{$item['id']}').attr('readonly','readonly');
            ";
            }
        }
        ?>

        //监听提交
        form.on('submit(addBtn)', function(data) {
            $.post('', data.field, function(rs) {
                if (rs.code == 1) {
                    parent.location.reload();
                } else {
                    layer.alert(rs.msg)
                }
            })
            return false;
        });
    });
</script>

{include file="common/footer"}