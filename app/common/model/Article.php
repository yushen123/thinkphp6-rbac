<?php 
declare (strict_types = 1);

namespace app\common\model;

class Article extends Base
{
    //查询条件
    protected $whereField = [
        'id' => ["=", "{value}"],
        'tenant_id' => ["=", "{value}"],
        'title' => ["like", "{value}%"],
        'thumb' => ["like", "{value}%"],
        'content' => ["like","{value}%"],
        'created_at' => ["between", "{value}"],
        'deleted_at' => ["between", "{value}"],
        'updated_at' => ["between", "{value}"],
        'phone'    => ["like", "{value}%"],
    ];

}
