<?php

namespace app\common\model;

class AdminUserRule extends Base
{
    protected $hidden = ['password'];


    /**
     * 添加
     * @param $data
     * @return bool
     */
    public function addData(array $data)
    {
        $rs = $this->save(['pid' => $data['pid'], 'type' => $data['type'], 'name' => $data['name'], 'rules' => $data['rules']]);
        return $rs;
    }

    /**
     * 菜单列表
     * @param int $pid
     * @param string $sort
     * @return \think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function listMenu(int $pid = 0,$type='menu', string $sort = 'id desc')
    {
        $where = ['pid'=>$pid];
        if(in_array($type,['menu','node'])){
            $where['type'] = $type;
        }
        $rs = $this->where($where)->order($sort)->select();
        return $rs;
    }

    /**
     * 判断名字是否存在
     * @param $name
     * @return bool
     */
    public function hasName($name){
        $rs = $this->where(['name'=>$name])->count();

        if($rs>0){
            return true;
        }else{
            return false;
        }
    }
    /**
     * 判断规则是否存在
     * @param string $rules
     * @return bool
     */
    public function hasRules(string $rules){
        $rs = $this->where(['rules'=>$rules])->count();
        //echo $this->getLastSql();exit();
        if($rs>0){
            return true;
        }else{
            return false;
        }
    }

}
