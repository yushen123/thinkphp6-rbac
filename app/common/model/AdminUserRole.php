<?php

namespace app\common\model;

class AdminUserRole extends Base
{
    protected $hidden = ['password'];


    /**
     * 添加
     * @param $data
     * @return bool
     */
    public function addData(array $data):int
    {
        $rs = $this->save(['pid' => $data['pid'], 'name' => $data['name'], 'sort' => intval($data['sort'])]);
        if($rs){
            return $this->id;
        }else{
            return  0;
        }
    }

    /**
     * 菜单列表
     * @param int $pid
     * @param string $sort
     * @return \think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function listMenu(int $pid = 0)
    {
        $where = ['pid' => $pid];

        $rs = $this->where($where)->order('sort desc')->select();
        return $rs;
    }

    /**
     * 获取父权限节点
     *
     * @param integer $id
     * @return string
     */
    public function parentRuleIds(int $pid): array
    {
        $rs = $this->field('pid,rule_ids')->where('id', '=', $pid)->find();
        if($rs){
            return explode(',', rtrim($rs['rule_ids'], ','));
        }else{
            return [];
        }
        
    }
}
