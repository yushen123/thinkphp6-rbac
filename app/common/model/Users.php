<?php

namespace app\common\model;


class Users extends Base
{
    //查询条件
    protected $whereField = [
        'username' => ["like", "{value}%"],
        'phone'    => ["like", "{value}%"],
    ];

}
