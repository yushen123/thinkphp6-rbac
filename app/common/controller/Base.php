<?php

namespace app\common\controller;

use app\traits\Jump;
use think\App;
use think\exception\ValidateException;
use think\Validate;

//接口文档生成工具目录:_doc/build_apidoc/buildapi.bat

//定义apidoc全局变量
/**
 * @apiDefine hasToken
 * @apiHeader {string} tenantid 租户id
 * @apiHeader {string} token 用户token
 * @apiSuccess {bool} data.status 返回状态码:true=成功,false=失败
 * @apiSuccess {string} data.message 返回中文提示信息
 * @apiSuccess {json} data.data 返回数据
 */

/**
 * @apiDefine hasAdminToken
 * @apiHeader {string} tenantid 租户id
 * @apiHeader {string} admintoken 用户token
 * @apiSuccess {bool} data.status 返回状态码:true=成功,false=失败
 * @apiSuccess {string} data.message 返回中文提示信息
 * @apiSuccess {json} data.data 返回数据
 */

/**
 * @apiDefine noToken
 * @apiHeader {string} tenantid 租户id
 * @apiSuccess {number} data.code 返回状态码:200=成功,其他=失败
 * @apiSuccess {string} data.msg 返回中文提示信息
 * @apiSuccess {json} data.data 数据
 */

/**
 * 控制器基础类
 */
abstract class Base
{
    use Jump;

    /**
     * Request实例
     * @var \think\Request
     */
    protected $request;

    /**
     * 应用实例
     * @var \think\App
     */
    protected $app;

    /**
     * 是否批量验证
     * @var bool
     */
    protected $batchValidate = false;

    /**
     * 控制器中间件
     * @var array
     */
    protected $middleware = [];

    /**
     * 构造方法
     * @access public
     * @param App $app 应用对象
     */
    public function __construct(App $app)
    {
        $this->app = $app;
        $this->request = $this->app->request;

        // 控制器初始化
        $this->initialize();
    }

    // 初始化
    protected function initialize()
    {
    }

    /**
     * 验证数据
     * @access protected
     * @param array $data 数据
     * @param string|array $validate 验证器名或者验证规则数组
     * @param array $message 提示信息
     * @param bool $batch 是否批量验证
     * @return array|string|true
     * @throws ValidateException
     */
    protected function validate(array $data, $validate, array $message = [], bool $batch = false)
    {
        if (is_array($validate)) {
            $v = new Validate();
            $v->rule($validate);
        } else {
            if (strpos($validate, '.')) {
                // 支持场景
                [$validate, $scene] = explode('.', $validate);
            }
            $class = false !== strpos($validate, '\\') ? $validate : $this->app->parseClass('validate', $validate);
            $v = new $class();
            if (!empty($scene)) {
                $v->scene($scene);
            }
        }

        $v->message($message);

        // 是否批量验证
        if ($batch || $this->batchValidate) {
            $v->batch(true);
        }

        return $v->failException(true)->check($data);
    }

}
