<?php
declare (strict_types = 1);

namespace app\admin\controller;

use think\facade\View;

class Index extends AdminBase
{

    public function index()
    {

        return View::fetch();
    }



}
