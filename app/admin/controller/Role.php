<?php

namespace app\admin\controller;

use app\common\model\AdminUserRole;
use app\Request;
use think\facade\View;

class Role extends AdminBase
{

    public function index()
    {
        return View::fetch();
    }

    public function add(Request $request)
    {

        if ($request->isPost()) {
            $roleModel = new AdminUserRole();
            $rs = $roleModel->addData($request->post());
            if ($rs) {
               return $this->success('添加成功','./selectRule?roleId='.$rs);
            } else {
                return $this->error('添加失败');
            }
        } else {
            $roleModel = new AdminUserRole();


            return View::fetch();
        }
    }

    /**
     * 选择权限节点
     *
     * @param Request $request
     * @return void
     */
    public function selectRule(Request $request)
    {
        $roleId = $request->request('roleId');
        if (!$roleId) {
            return $this->error('角色id丢失');
        }
        $roleModel = new AdminUserRole();
        $roleInfo = $roleModel->find($roleId);
        if (!$roleInfo) {
            return $this->error('角色不存在');
        }

        $this->adminId = intval($request->adminTokenValue['uid']);

        $parentRoles = $roleModel->parentRuleIds($roleInfo['pid']);
       // print_r($parentRoles);
        if ($request->isPost()) {

            if ($roleInfo['rule_ids'] == '*') {
                return $this->error('超级管理员不能编辑');
            }
            $roleInfo->rule_ids = str_replace('-1,', '', $request->post('ruleIds'));
            $rs = $roleInfo->save();
            if ($rs) {
                //清理redis缓存
                clearRedisCache('*:rules_*');
                clearRedisCache('*:menu_html:*_*');
                return $this->success("保存成功");
            } else {
                return $this->error("操作失败");
            }
        } else {
            View::assign('roleInfo', $roleInfo);
            $selected = explode(',', rtrim($roleInfo['rule_ids'],','));
            $ruleJson = adminRuleTree(0, $selected, $parentRoles);
            View::assign('ruleJson', $ruleJson);

            return View::fetch();
        }
    }


    /**
     * 编辑
     * @param Request $request
     * @return string|void
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function edit(Request $request)
    {
        $id = $request->request('id', 0, 'intval');
        $roleModel = new AdminUserRole();
        $roleInfo = $roleModel->find($id);
        if (!$roleInfo) {
            $this->error('角色不存在');
        }
        if ($request->isPost()) {
            $data = $request->post();

            $rs = $roleModel->where(['id' => $id])->update(['pid' => intval($data['pid']), 'name' => $data['name']]);
            if ($rs!== false) {
                return $this->success('保存成功','./selectRule?roleId='.$id);
            } else {
                return $this->success('无更新内容');
            }
        } else {

            View::assign('roleInfo', $roleInfo);

            return View::fetch();
        }
    }

    /**
     * 删除
     * @param Request $request
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function delete(Request $request){
        $id = $request->post('id', 0, 'intval');

        $roleModel = new AdminUserRole();
        $roleInfo = $roleModel->find($id);
        if (!$roleInfo) {
            return $this->error('角色不存在');
        }

        //查询是否有子节点
        if($roleModel->where(['pid'=>$id])->count()){
            return $this->error('请先删除子角色');
        }

        if($roleInfo->delete()){
            return $this->success('删除成功');
        }else{
            return  $this->error('操作失败');
        }
    }
}
