<?php
declare (strict_types = 1);

namespace app\admin\controller;

use app\common\controller\Base;
use app\middleware\CheckAdminLogin;
use think\App;

abstract class AdminBase extends Base
{
    protected $middleware = [
        CheckAdminLogin::class => ['*']
    ];

}
