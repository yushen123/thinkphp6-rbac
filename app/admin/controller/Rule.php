<?php

namespace app\admin\controller;

use app\common\model\AdminUserRule;
use app\Request;
use app\validate\AdminRule;
use think\exception\ValidateException;
use think\facade\View;

class Rule extends AdminBase
{


    public function index()
    {
        return View::fetch();
    }

    public function add(Request $request)
    {

        if ($request->isPost()) {
            $ruleModel = new AdminUserRule();
            try{
                validate(AdminRule::class)->scene('add')->rule(['rules'=>function($value)use ($ruleModel){
                    return $ruleModel->hasRules($value)?'规则已经存在:'.$value:true;
                }])->check($request->post());
            }catch (ValidateException $e){
                return $this->error($e->getMessage());
            }
            $rs = $ruleModel->addData($request->post());
            if ($rs) {
                //清除菜单缓存
                clearRedisCache('admin:menu_html:*');
                return $this->success('添加成功');
            } else {
                return $this->error('添加失败');
            }
        } else {
            $rule = new AdminUserRule();
            View::assign('menuList', $rule->listMenu(0));//一级菜单

            return View::fetch();
        }
    }

    /**
     *  添加
     * @param Request $request
     * @return string|void
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function addRule(Request $request)
    {
        $ruleModel = new AdminUserRule();
        if ($request->isPost()) {
            $data = $request->post();
            $rules = $request->post('rules');
            if (strpos($rules, '://') === false) {
                $rules = rtrim($rules, '/');
                $tmp = explode('/', $rules);
                if (count($tmp) != 4) {
                    return $this->error('节点格式错误!必须为:/admin/控制器/方法');
                }
                if ($ruleModel->where(['rules' => $rules])->count()) {
                    return $this->error('节点链接已存在：'.$rules);
                }
                $data['rules'] = trim($rules);
            }

            $rs = $ruleModel->addData($data);
            if ($rs) {
                return $this->success('添加成功');
            } else {
                return $this->error('添加失败');
            }
        } else {
            View::assign('menuList', $ruleModel->listMenu(0));//一级菜单

            return View::fetch();
        }
    }

    /**
     * 编辑
     * @param Request $request
     * @return string|void
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function editRule(Request $request)
    {
        $id = $request->request('id', 0, 'intval');
        $ruleModel = new AdminUserRule();
        $ruleInfo = $ruleModel->find($id);
        if (!$ruleInfo) {
            $this->error('节点不存在');
        }
        if ($request->isPost()) {
            $data = $request->post();
            $rules = $request->post('rules');
            if (strpos($rules, '://') === false) {
                $rules = rtrim($rules, '/');
                $tmp = explode('/', $rules);
                if (count($tmp) != 4) {
                    return $this->error('节点格式错误!必须为:/admin/控制器/方法');
                }
                if ($ruleModel->where(['rules' => $rules])->where('id', '<>', $id)->count()) {
                    return $this->error('节点链接已存在：'.$rules);
                }
                $data['rules'] = trim($rules);
            }
            $rs = $ruleModel->where(['id' => $id])->update(['rules' => $data['rules'],'pid'=>$data['pid'], 'name' => $data['name']]);
            if ($rs) {
                //清除菜单缓存
                clearRedisCache('admin:menu_html:*');
                return $this->success('编辑成功');
            } else {
                return $this->success('无更新内容');
            }
        } else {

            View::assign('ruleInfo', $ruleInfo);
            View::assign('menuList', $ruleModel->listMenu(0));//一级菜单

            return View::fetch();
        }
    }

    /**
     * 删除
     * @param Request $request
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function delete(Request $request){
        $id = $request->post('id', 0, 'intval');

        $ruleModel = new AdminUserRule();
        $ruleInfo = $ruleModel->find($id);
        if (!$ruleInfo) {
           return $this->error('节点不存在');
        }

        //查询是否有子节点
        if($ruleModel->where(['pid'=>$id])->count()){
            return $this->error('请先删除子节点');
        }

        if($ruleInfo->delete()){
            //清除菜单缓存
            clearRedisCache('admin:menu_html:*');
            return $this->success('删除成功');
        }else{
            return  $this->error('操作失败');
        }
    }

}
