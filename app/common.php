<?php
// 应用公共文件
use think\facade\Cache;

/**
 * 格式化输出json
 * @param bool $status 返回状态:true or false
 * @param string $message 提示文字
 * @param array $data 返回数据
 * @param int $httpStatus
 * @return \think\response\Json
 */
function ajaxData(bool $status, string $message = "", array $data = [], int $httpStatus = 200)
{

    $data = [
        "status" => $status,
        "msg" => $message,
        "data" => $data
    ];
    return json($data, $httpStatus);
}

/**
 * 获取redis句柄
 * @return object
 */
function initRedis()
{
    return Cache::store('redis')->handler();
}

/**
 * 生成加密字符串
 * @param $str 明文
 * @return string 密文
 */
function buildPassword(string $str): string
{
    $rs = md5($str . config('passsword.salt'));
    return $rs;
}

/**
 * 错误提示页
 */
function error(string $msg, string $url = '/')
{
    $msg = str_replace('"', '\"', $msg);
    $html = "<p>{$msg}</p>";
    $html .= "<a href='{$url}'>确定</a>";
    echo $html;
    exit();
}
