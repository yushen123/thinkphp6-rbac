<?php

namespace app\traits;

use think\Container;
use think\exception\HttpResponseException;
use think\facade\View;
use think\Response;

trait Jump
{
    /**
     * 应用实例
     * @var \think\App
     */
    protected $app;

    /**
     * 操作成功跳转的快捷方法
     * @access protected
     * @param  mixed     $msg 提示信息
     * @param  string    $url 跳转的URL地址
     * @param  int     $httpCode http状态码
     * @param  int   $wait 跳转等待时间
     * @param  array     $header 发送的Header信息
     * @return void
     */
    protected function success($msg = '', $url = null,int $httpCode = 200,int $wait = 3, array $header = [])
    {
        if (is_null($url) && isset($_SERVER["HTTP_REFERER"])) {
            $url = $_SERVER["HTTP_REFERER"];
        }

        $result = [
            'code' => 1,
            'msg'  => $msg,
            'url'  => $url,
            'wait' => $wait,
        ];

        $type = $this->getResponseType();
        // 把跳转模板的渲染下沉，这样在 response_send 行为里通过getData()获得的数据是一致性的格式

        if ('html' == strtolower($type)) {
            $data = View::fetch( config('app.dispatch_error_tmpl'), $result);
            $response = Response::create($data, $type, $httpCode)->header($header);
            throw new HttpResponseException($response);
            return ;
        }else{
            return json($result);
        }
    }

    /**
     * 操作错误跳转的快捷方法
     * @access protected
     * @param  mixed     $msg 提示信息
     * @param  string    $url 跳转的URL地址
     * @param  int     $httpCode http状态码
     * @param  int   $wait 跳转等待时间
     * @param  array     $header 发送的Header信息
     * @return void
     */
    protected function error($msg = '', $url = null, int $httpCode = 200,int $wait = 3, array $header = [])
    {
        $type = $this->getResponseType();
        if (is_null($url)) {
            $url = $this->app['request']->isAjax() ? '' : 'javascript:history.back(-1);';
        } elseif ('' !== $url) {
            $url = (strpos($url, '://') || 0 === strpos($url, '/')) ? $url : $this->app['url']->build($url);
        }

        $result = [
            'code' => 0,
            'msg'  => $msg,
            'url'  => $url,
            'wait' => $wait,
        ];

        if ('html' == strtolower($type)) {
            $data = View::fetch( config('app.dispatch_error_tmpl'), $result);
            $response = Response::create($data, $type, $httpCode)->header($header);
            throw new HttpResponseException($response);
            return ;
        }else{
            $response = Response::create($result, $type, $httpCode)->header($header);
            throw new HttpResponseException($response);
            return json($result);
        }
    }

    /**
     * 返回封装后的API数据到客户端
     * @access protected
     * @param  mixed     $data 要返回的数据
     * @param  integer   $code 返回的code
     * @param  mixed     $msg 提示信息
     * @param  string    $type 返回数据格式
     * @param  array     $header 发送的Header信息
     * @return void
     */
    protected function result($data, $code = 0, $msg = '', $type = '', array $header = [])
    {
        $result = [
            'code' => $code,
            'msg'  => $msg,
            'time' => time(),
            'data' => $data,
        ];

        $type     = $type ?: $this->getResponseType();
        $response = Response::create($result, $type)->header($header);

        throw new HttpResponseException($response);
    }

    /**
     * 获取当前的response 输出类型
     * @access protected
     * @return string
     */
    protected function getResponseType  ():string
    {
        if (!$this->app) {
            $this->app = app('app');
        }

        $isAjax = $this->app['request']->isAjax();

        return $isAjax
            ? 'json'
            : 'html';
    }
}
