<?php

namespace app\validate;

use think\Validate;

class AdminUser extends Validate
{
    protected $rule = [
        'username' => 'require|alphaDash|length:3,30',
        'password' => 'require|length:6,30',
        'role_ids' => 'require|number'
    ];

    //验证场景
    protected $scene = [
        'add'   => ['username', 'password', 'role_ids'],
        'login' => ['username', 'password'],
        'edit'  => ['username','role_ids', 'password'],
        'edit_nopwd'  => ['username','role_ids']
    ];

}