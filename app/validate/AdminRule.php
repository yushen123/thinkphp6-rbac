<?php
namespace app\validate;

use think\Validate;

class AdminRule extends Validate
{
    protected $rule = [
        'name'  =>  'require|chsDash|length:2,20',
        'rules' =>  'require|alphaDash|length:2,50',
    ];

    protected $scene = [
        'add' => ['name','rules','age']
    ];


}