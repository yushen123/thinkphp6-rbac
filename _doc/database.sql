/*
 Navicat Premium Data Transfer

 Source Server         : online test
 Source Server Type    : MySQL
 Source Server Version : 80016
 Source Host           : rm-2zed4030svwg98526no.mysql.rds.aliyuncs.com:3306
 Source Schema         : thinkphp6-rbac

 Target Server Type    : MySQL
 Target Server Version : 80016
 File Encoding         : 65001

 Date: 21/07/2020 16:32:37
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin_user
-- ----------------------------
DROP TABLE IF EXISTS `admin_user`;
CREATE TABLE `admin_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tenant_id` int(11) NOT NULL DEFAULT 0 COMMENT '租户id',
  `role_ids` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '角色id(多个用户逗号分隔)',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `status` tinyint(4) NULL DEFAULT 1 COMMENT '状态:-1=锁定;0=未激活;1=已激活',
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `activity_at` datetime(0) NULL DEFAULT NULL COMMENT '活跃时间',
  `deleted_at` datetime(0) NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username_unique`(`tenant_id`, `username`, `deleted_at`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of admin_user
-- ----------------------------
INSERT INTO `admin_user` VALUES (13, 1, '2', 'sysadmin', '0b4e7a0e5fe84ad35fb5f95b9ceeac79', 1, '2020-05-28 15:38:43', '2020-07-21 16:11:11', '2020-07-21 16:11:11', '0000-00-00 00:00:00');
INSERT INTO `admin_user` VALUES (23, 1, '34', 'admin', '0b4e7a0e5fe84ad35fb5f95b9ceeac79', 1, '2020-07-06 23:42:06', '2020-07-20 23:20:00', '2020-07-20 23:19:59', '0000-00-00 00:00:00');
INSERT INTO `admin_user` VALUES (24, 1, '41', 'editer', '0b4e7a0e5fe84ad35fb5f95b9ceeac79', 1, '2020-07-17 17:20:30', '2020-07-17 17:21:51', NULL, '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for admin_user_role
-- ----------------------------
DROP TABLE IF EXISTS `admin_user_role`;
CREATE TABLE `admin_user_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tenant_id` int(11) NOT NULL DEFAULT 0 COMMENT '租户id',
  `pid` int(11) NULL DEFAULT NULL COMMENT '父id',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '分组名称',
  `rule_ids` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '规则ID(多个用逗号分隔)',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序(逆序)',
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `tenant_id`(`tenant_id`, `name`, `deleted_at`) USING BTREE,
  INDEX `deleted_at`(`deleted_at`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 44 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户角色' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of admin_user_role
-- ----------------------------
INSERT INTO `admin_user_role` VALUES (2, 1, 0, '超级管理员', '*', 1, '2020-06-02 13:26:44', '2020-06-03 17:30:47', '0000-00-00 00:00:00');
INSERT INTO `admin_user_role` VALUES (9, 1, 2, '普通管理员', '34,37,46,44,', 0, '2020-06-02 13:37:24', '2020-07-03 23:32:20', '2020-07-03 23:32:20');
INSERT INTO `admin_user_role` VALUES (10, 1, 9, '小编', '34,36,38,', 0, '2020-06-02 13:40:44', '2020-07-03 23:32:14', '2020-07-03 23:32:14');
INSERT INTO `admin_user_role` VALUES (11, 1, 10, '小小编', '34,36,38,', 0, '2020-06-02 13:47:56', '2020-07-03 23:32:08', '2020-07-03 23:32:08');
INSERT INTO `admin_user_role` VALUES (12, 1, 9, '大编', '', 0, '2020-06-02 13:48:13', '2020-07-03 23:31:53', '2020-07-03 23:31:53');
INSERT INTO `admin_user_role` VALUES (13, 1, 2, '协会管理员', '40,41,39,42,34,37,38,36,', 0, '2020-06-02 13:48:44', '2020-07-03 23:31:38', '2020-07-03 23:31:38');
INSERT INTO `admin_user_role` VALUES (14, 1, 13, '处长', '34,36,', 0, '2020-06-02 13:50:25', '2020-07-03 23:31:25', '2020-07-03 23:31:25');
INSERT INTO `admin_user_role` VALUES (15, 1, 14, '科长', '', 0, '2020-06-02 13:50:33', '2020-07-03 23:31:18', '2020-07-03 23:31:18');
INSERT INTO `admin_user_role` VALUES (16, 1, 2, '小小管理员', '', 0, '2020-06-25 22:50:14', '2020-07-03 23:31:31', '2020-07-03 23:31:31');
INSERT INTO `admin_user_role` VALUES (31, 1, 10, '啊啊', '34,36,38,', 0, '2020-07-03 00:28:56', '2020-07-03 23:31:59', '2020-07-03 23:31:59');
INSERT INTO `admin_user_role` VALUES (32, 1, 11, 'bbb', NULL, 0, '2020-07-03 00:29:15', '2020-07-03 23:32:04', '2020-07-03 23:32:04');
INSERT INTO `admin_user_role` VALUES (33, 1, 12, 'da', '', 0, '2020-07-03 00:29:36', '2020-07-03 23:31:46', '2020-07-03 23:31:46');
INSERT INTO `admin_user_role` VALUES (34, 1, 2, '管理员', '93,82,83,89,88,81,92,91,90,34,54,80,79,78,37,46,44,36,95,94,38,', 0, '2020-07-06 21:36:44', '2020-07-15 15:15:26', '0000-00-00 00:00:00');
INSERT INTO `admin_user_role` VALUES (41, 1, 34, '小编', '76,77,', 0, '2020-07-06 22:26:31', '2020-07-06 22:26:36', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for admin_user_rule
-- ----------------------------
DROP TABLE IF EXISTS `admin_user_rule`;
CREATE TABLE `admin_user_rule`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tenant_id` int(11) NOT NULL COMMENT '租户id',
  `pid` int(11) NULL DEFAULT 0 COMMENT '父id',
  `type` enum('menu','node') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'menu' COMMENT '类型:menu=菜单;node=节点',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `rules` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '规则',
  `created_at` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updated_at` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `deleted_at` datetime(0) NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `rules_uniq`(`tenant_id`, `rules`, `deleted_at`) USING BTREE,
  INDEX `deleted_at`(`deleted_at`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 98 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '权限节点表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of admin_user_rule
-- ----------------------------
INSERT INTO `admin_user_rule` VALUES (34, 1, 0, 'menu', '权限管理', '/admin/role/index?', '2020-06-02 10:59:50', '2020-06-02 10:59:50', '0000-00-00 00:00:00');
INSERT INTO `admin_user_rule` VALUES (36, 1, 34, 'menu', '菜单&节点', '/admin/rule/index', '2020-06-02 11:01:25', '2020-06-02 11:01:25', '0000-00-00 00:00:00');
INSERT INTO `admin_user_rule` VALUES (37, 1, 34, 'menu', '角色管理', '/admin/role/index', '2020-06-02 11:01:53', '2020-06-02 11:01:53', '0000-00-00 00:00:00');
INSERT INTO `admin_user_rule` VALUES (38, 1, 36, 'node', '添加节点', '/admin/rule/add', '2020-06-03 14:51:26', '2020-06-03 14:51:26', '0000-00-00 00:00:00');
INSERT INTO `admin_user_rule` VALUES (39, 1, 0, 'menu', '首页管理', '/admin/index/index', '2020-06-03 15:09:01', '2020-07-02 00:48:25', '2020-07-02 00:48:25');
INSERT INTO `admin_user_rule` VALUES (40, 1, 34, 'menu', '内容管理', '/admin/content', '2020-06-03 15:09:57', '2020-07-02 00:39:05', '2020-07-02 00:39:05');
INSERT INTO `admin_user_rule` VALUES (41, 1, 34, 'menu', '内容列表', '/admin/content/list', '2020-06-03 15:10:43', '2020-07-02 00:39:25', '2020-07-02 00:39:25');
INSERT INTO `admin_user_rule` VALUES (42, 1, 39, 'menu', '广告', '/admin/index/ad', '2020-06-03 15:11:06', '2020-07-02 00:48:19', '2020-07-02 00:48:19');
INSERT INTO `admin_user_rule` VALUES (44, 1, 37, 'node', '添加角色', '/admin/role/add', '2020-06-04 11:32:11', '2020-06-04 11:32:11', '0000-00-00 00:00:00');
INSERT INTO `admin_user_rule` VALUES (45, 1, 39, 'menu', '枫桥居', 'http://www.fengqiaoju.com/', '2020-06-10 19:51:34', '2020-07-02 00:48:14', '2020-07-02 00:48:14');
INSERT INTO `admin_user_rule` VALUES (46, 1, 37, 'node', '配置角色节点', '/admin/role/selectRule', '2020-06-11 10:10:06', '2020-06-11 10:10:06', '0000-00-00 00:00:00');
INSERT INTO `admin_user_rule` VALUES (50, 1, 39, 'menu', '首页', '/admin/index/index?', '2020-06-12 17:57:48', '2020-07-02 00:48:02', '2020-07-02 00:48:02');
INSERT INTO `admin_user_rule` VALUES (54, 1, 34, 'menu', '用户管理', '/admin/user/index', '2020-07-03 23:40:01', '2020-07-03 23:40:01', '0000-00-00 00:00:00');
INSERT INTO `admin_user_rule` VALUES (58, 1, 0, 'menu', '菜单', '/admin/', '2020-07-04 23:21:47', '2020-07-05 00:05:26', '2020-07-05 00:05:26');
INSERT INTO `admin_user_rule` VALUES (59, 1, 0, 'menu', 'aaaa', 'a', '2020-07-04 23:48:09', '2020-07-05 01:04:19', '2020-07-05 01:04:19');
INSERT INTO `admin_user_rule` VALUES (60, 1, 0, 'menu', 'aa', 'aa', '2020-07-04 23:48:43', '2020-07-05 01:04:12', '2020-07-05 01:04:12');
INSERT INTO `admin_user_rule` VALUES (72, 1, 0, 'menu', '菜单', '/admin', '2020-07-05 00:05:43', '2020-07-05 00:05:59', '2020-07-05 00:05:59');
INSERT INTO `admin_user_rule` VALUES (75, 1, 0, 'menu', '菜单', '/admin', '2020-07-05 01:03:15', '2020-07-05 01:03:28', '2020-07-05 01:03:28');
INSERT INTO `admin_user_rule` VALUES (76, 1, 0, 'menu', '常规管理', '/admin/index/?', '2020-07-05 01:03:40', '2020-07-14 22:57:46', '2020-07-14 22:57:46');
INSERT INTO `admin_user_rule` VALUES (77, 1, 76, 'menu', '首页', '/admin/index/index', '2020-07-05 01:08:10', '2020-07-14 22:57:24', '2020-07-14 22:57:24');
INSERT INTO `admin_user_rule` VALUES (78, 1, 54, 'node', '添加', '/admin/user/add', '2020-07-09 11:39:25', '2020-07-09 11:39:25', '0000-00-00 00:00:00');
INSERT INTO `admin_user_rule` VALUES (79, 1, 54, 'node', '编辑', '/admin/user/edit', '2020-07-09 11:39:43', '2020-07-09 11:39:43', '0000-00-00 00:00:00');
INSERT INTO `admin_user_rule` VALUES (80, 1, 54, 'node', '删除', '/admin/user/delete', '2020-07-09 11:40:59', '2020-07-09 11:40:59', '0000-00-00 00:00:00');
INSERT INTO `admin_user_rule` VALUES (81, 1, 82, 'menu', '生成CRUD', '/admin/crud/index', '2020-07-09 11:52:41', '2020-07-09 11:52:41', '0000-00-00 00:00:00');
INSERT INTO `admin_user_rule` VALUES (82, 1, 0, 'menu', '工具', '/admin/tool/index', '2020-07-09 13:56:21', '2020-07-09 13:56:21', '0000-00-00 00:00:00');
INSERT INTO `admin_user_rule` VALUES (83, 1, 82, 'menu', '生成节点', '/admin/tool/buildRule', '2020-07-09 13:58:22', '2020-07-09 13:58:22', '0000-00-00 00:00:00');
INSERT INTO `admin_user_rule` VALUES (84, 1, 77, 'node', '添加', '/admin/index/add', '2020-07-10 13:49:41', '2020-07-14 22:56:47', '2020-07-14 22:56:47');
INSERT INTO `admin_user_rule` VALUES (85, 1, 77, 'node', '删除', '/admin/index/delete', '2020-07-10 13:49:41', '2020-07-14 22:57:02', '2020-07-14 22:57:02');
INSERT INTO `admin_user_rule` VALUES (86, 1, 77, 'node', '编辑', '/admin/index/edit', '2020-07-10 13:49:42', '2020-07-14 22:57:13', '2020-07-14 22:57:13');
INSERT INTO `admin_user_rule` VALUES (87, 1, 34, 'node', '生成', '/admin/crud/build', '2020-07-10 14:04:38', '2020-07-15 15:10:18', '2020-07-15 15:10:18');
INSERT INTO `admin_user_rule` VALUES (88, 1, 83, 'node', '删除', '/admin/tool/delete', '2020-07-10 14:04:38', '2020-07-10 14:04:38', '0000-00-00 00:00:00');
INSERT INTO `admin_user_rule` VALUES (89, 1, 83, 'node', '编辑', '/admin/tool/edit', '2020-07-10 14:04:38', '2020-07-10 14:04:38', '0000-00-00 00:00:00');
INSERT INTO `admin_user_rule` VALUES (90, 1, 81, 'node', '生成', '/admin/crud/build', '2020-07-10 14:44:29', '2020-07-10 14:44:29', '0000-00-00 00:00:00');
INSERT INTO `admin_user_rule` VALUES (91, 1, 81, 'node', '删除', '/admin/crud/delete', '2020-07-10 14:44:29', '2020-07-10 14:44:29', '0000-00-00 00:00:00');
INSERT INTO `admin_user_rule` VALUES (92, 1, 81, 'node', '编辑', '/admin/crud/edit', '2020-07-10 14:44:29', '2020-07-10 14:44:29', '0000-00-00 00:00:00');
INSERT INTO `admin_user_rule` VALUES (93, 1, 96, 'menu', '首页', '/admin/index/index', '2020-07-14 23:01:37', '2020-07-14 23:01:37', '0000-00-00 00:00:00');
INSERT INTO `admin_user_rule` VALUES (94, 1, 36, 'node', '删除', '/admin/rule/delete', '2020-07-15 15:06:29', '2020-07-15 15:06:29', '0000-00-00 00:00:00');
INSERT INTO `admin_user_rule` VALUES (95, 1, 36, 'node', '编辑', '/admin/rule/edit', '2020-07-15 15:06:29', '2020-07-15 15:06:29', '0000-00-00 00:00:00');
INSERT INTO `admin_user_rule` VALUES (96, 1, 0, 'menu', '常规管理', '/admin/index/index?', '2020-07-15 23:42:14', '2020-07-15 23:42:14', '0000-00-00 00:00:00');
INSERT INTO `admin_user_rule` VALUES (97, 1, 96, 'menu', '用户管理', '/admin/users/index', '2020-07-15 23:42:57', '2020-07-21 16:00:07', '2020-07-21 16:00:07');

SET FOREIGN_KEY_CHECKS = 1;
