thinkphp6-rbac多租户后台管理系统
===============

UI基于layui的后台权限管理系统。里面集成了菜单管理，权限管理，角色管理，用户管理和代码生成工具，方便快速生成thinkphp6的mvc代码。

 导入sql: 
 
 `_doc/database.sql`
 
 多租户配置:
 
 * 由于系统设计为多租户,可以在nginx配置环境变量TENANTID
 * 或者在public/index.php指定租户TENANTID

登录地址

`/admin/`

默认超级管理员账号: 

`sysadmin`

密码:

`aaaaaa`

### 开启调试模式 ###
应用默认是部署模式，在开发阶段，可以修改环境变量APP_DEBUG开启调试模式，上线部署后切换到部署模式。
本地开发的时候可以在应用根目录下面定义.env文件。

### 系统截图 ###

![截图](_doc/img/截图1.png)
![截图](_doc/img/截图2.png)